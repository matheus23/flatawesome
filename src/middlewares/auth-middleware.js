import _ from 'lodash'

import { AUTH_REQUEST, authSucceed, authFail } from '../reducers/auth'
import { storageSave } from '../reducers/storage-wrapper'

export default config => store => next => async action => {
    if (action.type !== AUTH_REQUEST) {
        return next(action)
    }

    const body = JSON.stringify({
        emailOrUsername: action.username,
        password: action.password,
        rememberMe: action.rememberMe
    })

    try {
        const authToken = store.getState().auth.token
        const response = await authFetch(config, '/api/v1/auth/login', body, authToken)
        const json = await response.json()

        if (response.ok) {
            store.dispatch(authSucceed(json))
            store.dispatch(storageSave(action.rememberMe ? 'localStorage' : 'sessionStorage', {
                auth: store.getState().auth
            }))
        } else {
            throw json
        }
    } catch (err) {
        store.dispatch(authFail(err))
    }
}

const authFetch = (config, route, body, authToken) => 
    fetch(config.server + route, {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        headers: {
            'content-type': 'application/json',
            'Authorization': 'Bearer ' + authToken
        },
        body
    })
