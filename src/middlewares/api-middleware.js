export const API_REQUEST = 'API_REQUEST'

export const apiRequest = (apiRequest, successType, failureType) => ({
    type: API_REQUEST,
    payload: apiRequest,
    meta: { successType, failureType }
})

export default baseUrl => store => next => async action => {
    if (action == null || action.type !== API_REQUEST) {
        return next(action)
    }

    const { route, ...requestOptions } = action.payload

    // Add a header, if we have an authentication token:
    const authToken = store.getState().auth.token
    if (authToken != null) {
        requestOptions.headers['Authorization'] = `Bearer ${authToken}`
    }

    try {
        const response = await fetch(baseUrl + route, requestOptions)
        const result = await response.json()

        if (!response.ok) {
            throw result
        }

        store.dispatch({
            type: action.meta.successType,
            payload: result
        })
    } catch (error) {
        store.dispatch({
            type: action.meta.failureType,
            payload: error
        })
    }
}
