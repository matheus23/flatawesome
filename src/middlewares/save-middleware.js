import { STORAGE_SAVE, storageKey } from '../reducers/storage-wrapper'

export default store => next => action => {
    if (action == null || action.type !== STORAGE_SAVE) {
        return next(action)
    }

    const storage = 
        action.meta.storageType === 'localStorage' ? window.localStorage :
        action.meta.storageType === 'sessionStorage' ? window.sessionStorage :
        null
    
    if (storage == null) {
        console.error('Unknown storage type: ', action.meta.storageType)
        return
    }

    storage.setItem(storageKey, JSON.stringify(action.payload))
}
