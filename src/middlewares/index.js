import configureAuthMiddelware from './auth-middleware'
import configureApiMiddleware from './api-middleware'
import saveMiddleware from './save-middleware'

export default () => {
    const baseUrl = 'http://localhost:9000'
    const authMiddleware = configureAuthMiddelware({ server: baseUrl })
    const apiMiddleware = configureApiMiddleware(baseUrl)
    return [
        authMiddleware,
        apiMiddleware,
        saveMiddleware
    ]
}
