import '@babel/polyfill'
import 'whatwg-fetch'

import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import { AppContainer } from 'react-hot-loader'

import { applyMiddlewareReplacable, composeEnhancers } from './util'
import reducer from './reducers/index'
import { loadFromStorage } from './reducers/storage-wrapper'
import getMiddlewares from './middlewares/index'
import Routes from './containers/Routes'

const store = createStore(
    reducer,
    composeEnhancers(
        applyMiddlewareReplacable(...getMiddlewares())
    )
)

loadFromStorage(store)

const reactMainElem = document.querySelector('#react-main')

const render = Component => {
    ReactDOM.render(
        <AppContainer>
            <Provider store={store}>
                <Component />
            </Provider>
        </AppContainer>
        , reactMainElem
    )
}

render(Routes)

if (module.hot) {
    module.hot.accept('./reducers/index', (outdatedModules) => {
        store.replaceReducer(reducer)
    })
    module.hot.accept('./middlewares/index', (outdatedModules) => {
        store.replaceMiddleware(...getMiddlewares())
    })
    module.hot.accept('./containers/Routes', (outdatedModules) => {
        render(Routes)
    })
}
