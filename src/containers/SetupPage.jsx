import React from 'react'

import { Redirect } from 'react-router-dom'

import PageLayout from '../presentationals/PageLayout'
import * as zenkit from '../zenkit-api'

import { WORKSPACES_SUCCESS, WORKSPACES_FAILURE, workspaceChosen, listChosen } from '../reducers/setup'
import { isLoggedIn } from '../reducers/auth'
import { apiRequest } from '../middlewares/api-middleware'
import WorkspaceList from '../presentationals/WorkspaceList'

export default class SetupPage extends React.Component {
    constructor(props) {
        super(props)

        const requestWorkspaces = apiRequest(
            zenkit.workspacesWithLists({}),
            WORKSPACES_SUCCESS,
            WORKSPACES_FAILURE
        )

        props.dispatch(requestWorkspaces)
    }

    handleAddWorkspace(event) {

    }

    handleChooseWorkspace(workspace) {
        this.props.dispatch(workspaceChosen(workspace))
    }

    handleChooseList(list) {
        this.props.dispatch(listChosen(list))
    }

    render() {
        const { setup, auth } = this.props

        return (
            <PageLayout>
                { isLoggedIn(auth) === false && <Redirect exact to='/login' /> }
                { setup.workspaces == null ?
                    'Loading...' :
                    <WorkspaceList
                        workspaces={setup.workspaces}
                        chosenWorkspace={setup.chosenWorkspace}
                        chosenList={setup.chosenList}
                        onAddWorkspaces={this.handleAddWorkspace.bind(this)}
                        onChooseWorkspace={this.handleChooseWorkspace.bind(this)}
                        onChooseList={this.handleChooseList.bind(this)}
                    />
                }
            </PageLayout>
        )
    }
}

