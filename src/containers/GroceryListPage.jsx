import React from 'react'

import { Link, Redirect } from 'react-router-dom'

import _ from 'lodash'

import AddGrocery from '../presentationals/AddGrocery'
import GroceryList from '../presentationals/GroceryList'
import PageLayout from '../presentationals/PageLayout'

import { addGrocery, removeGrocery, toggleGrocery } from '../reducers/groceries'
import { isLoggedIn } from '../reducers/auth'

export default ({ dispatch, groceries, auth }) => (
    !isLoggedIn(auth) ?
        <Redirect to='/login' /> :
        <PageLayout>
            <section className='grocery-list-container'>
                <AddGrocery 
                    onAddGrocery={(e, text) => {
                        e.preventDefault()
                        
                        const actualText = text.trim()
                        
                        if (_.isEmpty(actualText)) {
                            return text
                        } else {
                            dispatch(addGrocery(text))
                            return ''
                        }
                    }}
                />
                <GroceryList 
                    groceries={groceries}
                    onToggleGrocery={(event, index) => {
                        event.preventDefault()
                        event.stopPropagation()
                        dispatch(toggleGrocery(index))
                    }} 
                    onRemoveGrocery={(event, index) => {
                        event.preventDefault()
                        event.stopPropagation()
                        dispatch(removeGrocery(index))
                    }}
                />
            </section>
        </PageLayout>
)
