import React from 'react'

import { Link, Redirect } from 'react-router-dom'

import PageLayout from '../presentationals/PageLayout'
import { authRequest, isLoggedIn } from '../reducers/auth'

export default ({ dispatch, auth }) => (
    isLoggedIn(auth) ?
        <Redirect to='/' /> :
        <PageLayout>
            <Login
                onLogIn={(e, { username, password, rememberMe }) => {
                    e.preventDefault()
                    dispatch(authRequest(username, password, rememberMe))
                }}
                {...categorizeError(auth.lastError)}
            />
        </PageLayout>
)

const categorizeError = (lastError) => {
    if (lastError == null || lastError.description == null) {
        return {}
    }
    const description = lastError.description
    if (description.includes('username') || description.includes('email')) {
        return { usernameError: description }
    }
    if (description.includes('password')) {
        return { passwordError: description }
    }
    return { otherError: description }
}


class Login extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            username: '',
            rememberMe: false
        }
        this.passwordInput = null
    }

    handleUsernameChange(event) {
        this.setState({
            username: event.target.value
        })
    }

    handleRememberMeChange(event) {
        this.setState({
            rememberMe: event.target.value
        })
    }

    render() {
        const { onLogIn, usernameError, passwordError, otherError } = this.props
        return (
            <section className='login-container'>
                <form
                    className='login-form'
                    onSubmit={(e) => onLogIn(e, {
                        username: this.state.username,
                        password: this.passwordInput.value,
                        rememberMe: this.state.rememberMe
                    })}
                >
                    <h3>Log in to <a href='https://zenkit.com/'>Zenkit</a></h3>
                    <input 
                        type='text' 
                        placeholder='Email or Username' 
                        value={this.state.username}
                        onChange={(e) => this.handleUsernameChange(e)}
                    />
                    { usernameError && 
                        <span className='errorText'>{usernameError}</span>
                    }
                    <input
                        type='password' 
                        placeholder='Password'
                        ref={element => { this.passwordInput = element }}
                    />
                    { passwordError && 
                        <span className='errorText'>{passwordError}</span>
                    }
                    <label>
                        <input
                            type='checkbox'
                            value={this.state.rememberMe}
                            onChange={(e) => this.handleRememberMeChange(e)}
                        />Remember me
                    </label>
                    { otherError && 
                        <span className='errorText'>{otherError}</span>
                    }
                    <input type='submit' value='Log in' />
                </form>
            </section>
        )
    }
}
