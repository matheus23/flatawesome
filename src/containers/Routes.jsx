import React from 'react'

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { connect } from 'react-redux'

import '../main.scss'

import GroceryListPage from './GroceryListPage'
import LoginPage from './LoginPage'
import SetupPage from './SetupPage'

const Routes = (storeProps) => (
    <Router>
        <Switch>
            <Route exact path='/' render={props => <GroceryListPage {...storeProps} {...props} />} />
            <Route path='/login' render={props => <LoginPage {...storeProps} {...props} />} />
            <Route path='/setup' render={props => <SetupPage {...storeProps} {...props} />} />
        </Switch>
    </Router>
)

export default connect(state => state)(Routes)
