import React from 'react'
import classnames from 'classnames'

const GroceryList = ({ groceries, onRemoveGrocery, onToggleGrocery }) => (
    <ul className='grocery-list'>
        {
            groceries.map((grocery, index) => 
                <GroceryItem 
                    key={index}
                    grocery={grocery}
                    onClick={(e) => onToggleGrocery(e, index)}
                    onRemove={(e) => onRemoveGrocery(e, index)}
                />
            )
        }
    </ul>
)

const GroceryItem = ({ grocery, onRemove, onClick }) => (
    <li 
        className='grocery-item'
        onClick={(e) => onClick(e)}
    >
        <span
            className={classnames('grocery-text', {
                checked: grocery.checked
            })}
        >
            {grocery.text}
        </span>
        <button
            className='remove-grocery-button fa fa-times'
            onClick={(e) => onRemove(e)}
        />
    </li>
)

export default GroceryList
