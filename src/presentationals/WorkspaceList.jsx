import React from 'react'

import classnames from 'classnames'

export default ({ workspaces, chosenWorkspace, chosenList, onAddWorkspace, onChooseWorkspace, onChooseList }) => (
    <article className='workspace-list-container'>
        { workspaces.map(workspace => (
            <Workspace
                key={workspace.uuid}
                workspace={workspace}
                chosenWorkspace={chosenWorkspace}
                chosenList={chosenList}
                onChooseWorkspace={onChooseWorkspace}
                onChooseList={onChooseList}
            />
        )) }
    </article>
)

export const Workspace = ({ workspace, chosenWorkspace, chosenList, onChooseWorkspace, onChooseList }) => (
    <section className='workspace'>
        <article>
            <h2 onClick={(e) => {
                e.preventDefault()
                onChooseWorkspace(workspace)
            }}>{workspace.name}</h2>

            <section>
                { workspace.lists.map(list => (
                    <ListBlob
                        key={list.uuid}
                        name={list.name}
                        isChosen={chosenList && chosenList.uuid === list.uuid}
                        iconClasses={list.iconClassNames}
                        onClick={(e) => {
                            e.preventDefault()
                            if (!isWorkspaceChosen(workspace, chosenWorkspace)) {
                                onChooseWorkspace(workspace)
                            }
                            onChooseList(list)
                        }}
                    />
                ))}
                {/* <ListBlob
                    iconClasses='fa fa-plus'
                    name='Add a list'
                /> */}
            </section>
        </article>
        <div className={classnames('workspace-greyout', {
            'greyout-inactive': chosenWorkspace != null ? chosenWorkspace.id === workspace.id : true
        })} />
    </section>
)

const isWorkspaceChosen = (workspace, chosenWorkspace) => 
    (chosenWorkspace != null && chosenWorkspace.id === workspace.id)

export const ListBlob = ({ iconClasses, name, isChosen, onClick }) => {
    const spanClasses = classnames('icon', iconClasses)
    const figureClasses = classnames('list-blob', {
        chosen: isChosen
    })
    return (
        <figure className={figureClasses} onClick={onClick}>
            <section className='blob'>
                <span className={spanClasses}>
                    {iconClasses == null && name.slice(0, 2)}
                </span>
            </section>
            <figcaption>
                {name}
            </figcaption>
        </figure>
    )
}
