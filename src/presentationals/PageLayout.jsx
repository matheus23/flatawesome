import React from 'react'

import { Link } from 'react-router-dom'

export default ({ children, logout }) => (
    <>
        <header>
            <h1 className='banner'>
                <Link to='/setup'><span className='banner-flat'>Flat</span></Link>
                <Link to='/login'><span className='banner-awesome'>awesome</span></Link>
            </h1>

        </header>
        <main>
            {children}
        </main>
    </>
)
