import React from 'react'

export default class AddGrocery extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            value: ''
        }
    }

    handleChange(event) {
        this.setState({
            value: event.target.value
        })
    }

    handleSubmit(event) {
        const newText = this.props.onAddGrocery(event, this.state.value)
        if (_.isString(newText)) {
            this.setState({
                value: newText
            })
        }
    }

    render() {
        return (
            <form onSubmit={(e) => this.handleSubmit(e)}>
                <label>
                    What to buy:
                    <input 
                        className='add-grocery-input'
                        type='text' 
                        value={this.state.value} 
                        onChange={(e) => this.handleChange(e)}
                    />
                </label>
                <input className='add-grocery-button' type='submit' value='Add' />
            </form>
        )
    }
    
}
