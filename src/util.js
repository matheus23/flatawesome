import { createStore, compose } from 'redux'

export function applyMiddlewareReplacable(...middlewares) {
    return createStore => (...args) => {
        const store = createStore(...args)

        let dispatch = () => {
            throw new Error(
            `Dispatching while constructing your middleware is not allowed. ` +
                `Other middleware would not be applied to this dispatch.`
            )
        }

        const middlewareAPI = {
            getState: store.getState,
            dispatch: (...args) => dispatch(...args)
        }

        let chain = middlewares.map(middleware => middleware(middlewareAPI))
        dispatch = compose(...chain)(store.dispatch)

        return {
            ...store,
            dispatch: (...args) => dispatch(...args),
            replaceMiddleware: (...middlewares) => {
                chain = middlewares.map(middleware => middleware(middlewareAPI))
                dispatch = compose(...chain)(store.dispatch)
            }
        }
    }
}
  
export const composeEnhancers =
    typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
            // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
        }) :
        compose


