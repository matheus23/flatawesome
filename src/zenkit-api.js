import genUuidV4 from 'uuid/v4'

const requestConfig = {
    mode: 'cors',
    cache: 'no-cache',
    headers: {
        'content-type': 'application/json'
    }
}

const post = (route, body) => ({
    route,
    method: 'POST',
    ...requestConfig,
    body: body
})

const get = (route) => ({
    route,
    method: 'GET',
    ...requestConfig
})

/*
    API ROUTES
*/

export const listEntries = ({
    // required
    listId,
    // optional
    filter = { AND: { TERMS: [] }},
    limit = 100,
    skip = 0,
    taskStyle = false
}) => post(
    `/api/v1/lists/${listId}/entries/filter/list`,
    { filter, limit, skip, taskStyle }
)

export const addWorkspaceList = ({
    // required
    workspaceId,
    name,
    // optional
    templateId = 'blanc',
    description = '',
    uuid = genUuidV4()
}) => post(
    `/api/v1/workspaces/${workspaceAllId}/mini-templates/${templateId}`,
    { name, description, uuid }
)

export const addListEntry = ({
    // required
    listId,
    // optional
    displayString = '',
    sortOrder = 1,
    uuid = genUuidV4()
}) => post(
    `/api/v1/lists/${listId}/entries`,
    { displayString, sortOrder, uuid, [`${uuid}_text`]: displayString }
)

export const elementCategories = () => get(`/api/v1/elementcategories`)

export const currentUser = () => get(`/api/v1/auth/currentuser`)

export const organizations = ({ userId = 'me' }) => get(`/api/v1/users/${userId}/organizations`)

export const notifications = ({ userId = 'me' }) => get(`/api/v1/users/${userId}/new-notifications-in-minimal-format`)

export const workspaces = ({ userId = 'me' }) => get(`/api/v1/users/${userId}/workspaces`)

export const workspacesWithLists = ({ userId = 'me' }) => get(`/api/v1/users/${userId}/workspacesWithLists`)

export const listDetails = ({ listShortId, authToken }) => get(`/api/v1/lists/${listShortId}`)

export const listElements = ({ listId, authToken }) => get(`/api/v1/lists/${listId}/elements`)

export const tagsAndAssignments = ({ userId = 'me' }) => get(`/api/v1/users/${userId}/tags-and-assignments`)

export const listUsers = ({ listId, authToken }) => get(`/api/v1/lists/${listId}/users`)

export const workspaceUsers = ({ workspaceId, authToken }) => get(`/api/v1/workspaces/${workspaceId}/users`)
