const ADD_GROCERY = 'ADD_GROCERY'
const REMOVE_GROCERY = 'REMOVE_GROCERY'
const TOGGLE_GROCERY = 'TOGGLE_GROCERY'

export const addGrocery = (text) => ({
    type: ADD_GROCERY,
    text: text
})

export const removeGrocery = (index) => ({
    type: REMOVE_GROCERY,
    index: index
})

export const toggleGrocery = (index) => ({
    type: TOGGLE_GROCERY,
    index: index
})

export const initialState = []

export default (groceries = initialState, action) => {
    switch (action.type) {
        case ADD_GROCERY:
            return [...groceries, {
                text: action.text,
                checked: false
            }]
        case REMOVE_GROCERY:
            return groceries.filter(
                (grocery, index) => index !== action.index
            )
        case TOGGLE_GROCERY:
            return groceries.map((grocery, index) => 
                index === action.index ?
                    { ...grocery, checked: !grocery.checked } :
                    grocery
            )
        default:
            return groceries
    }
}
