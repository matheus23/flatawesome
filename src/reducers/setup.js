export const WORKSPACES_SUCCESS = 'WORKSPACES_SUCCESS'
export const WORKSPACES_FAILURE = 'WORKSPACES_FAILURE'

export const WORKSPACE_CHOSEN = 'WORKSPACE_CHOSEN'
export const LIST_CHOSEN = 'LIST_CHOSEN'

export const workspaceChosen = (workspace) => ({
    type: WORKSPACE_CHOSEN,
    payload: workspace
})

export const listChosen = (list) => ({
    type: LIST_CHOSEN,
    payload: list
})

export const initialState = {
    workspaces: [],
    chosenWorkspace: null,
    chosenList: null
}

export default (state = initialState, action) => {
    switch (action.type) {
    case WORKSPACES_SUCCESS:
        return {
            ...state,
            workspaces: action.payload.sort(compareWorkspaceCreationDate)
        }
    case WORKSPACES_FAILURE:
        // TODO: Yo... errors man!
        return state
    case WORKSPACE_CHOSEN:
        return {
            ...state,
            chosenWorkspace: action.payload
        }
    case LIST_CHOSEN:
        return {
            ...state,
            chosenList: action.payload
        }
    default:
        return state
    }
}

const compareWorkspaceCreationDate = (workspaceA, workspaceB) =>
    (new Date(workspaceA.created_at) > new Date(workspaceB.created_at))
