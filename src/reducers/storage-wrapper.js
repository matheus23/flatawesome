import merge from 'lodash/merge'

export const STORAGE_SAVE = 'STORAGE_SAVE'
export const STORAGE_LOADED = 'STORAGE_LOADED'

export const storageKey = 'flatawesome-state'

export const storageLoaded = (state) => ({
    type: STORAGE_LOADED,
    payload: state
})

export const storageSave = (storageType, state) => ({
    type: STORAGE_SAVE,
    payload: state,
    meta: {
        storageType
    }
})

export const storageWrapper = innerReducer => (state, action) => {
    if (action != null && action.type === STORAGE_LOADED) {
        return merge({}, state, action.payload)
    }

    return innerReducer(state, action)
}

export const loadFromStorage = (store) => {
    const local = localStorage.getItem(storageKey)
    const session = sessionStorage.getItem(storageKey)

    try {
        const fromLocal = local ? JSON.parse(local) : {}
        const fromSession = session ? JSON.parse(session) : {}

        store.dispatch(storageLoaded(merge({}, fromLocal, fromSession)))
    } catch (err) {
        console.error('Could not load from storage:', err, local, session)
    }
}
