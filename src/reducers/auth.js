export const AUTH_REQUEST = 'AUTH_REQUEST'
export const AUTH_SUCCESS = 'AUTH_SUCCES'
export const AUTH_FAILURE = 'AUTH_FAILURE'

export const authRequest = (user, password, rememberMe) => ({
    type: AUTH_REQUEST,
    username: user,
    password,
    rememberMe
})

export const authSucceed = (response) => ({
    type: AUTH_SUCCESS,
    data: response
})

export const authFail = (error) => ({
    type: AUTH_FAILURE,
    ...error
})

export const initialState = {
    user: null,
    token: null,
    didRegister: false,
    lastError: null
}

export const isLoggedIn = ({ token }) => token != null

export default (state = initialState, action) => {
    switch (action.type) {
        case AUTH_SUCCESS:
            return {
                ...state,
                user: action.data.user,
                token: action.data.token,
                didRegister: action.data.didRegister,
                lastError: null
            }
        case AUTH_FAILURE:
            return {
                ...state,
                lastError: action.error
            }
        default: 
            return state
    }
}
