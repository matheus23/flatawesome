import { combineReducers } from 'redux'

import groceries from './groceries'
import auth from './auth'
import setup from './setup'

import { storageWrapper } from './storage-wrapper'

export default storageWrapper(combineReducers({
    groceries,
    auth,
    setup
}))
