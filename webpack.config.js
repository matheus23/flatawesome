const path = require('path')
const convert = require('koa-connect')
const history = require('connect-history-api-fallback')

const packageJson = require('./package.json')

module.exports = {
    mode: process.env.WEBPACK_SERVE ? 'development' : 'production',
    entry: './src/main.jsx',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'main.bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: [
                        '@babel/preset-react', 
                        ['@babel/preset-env', {
                            /* We need to leave module bundling to webpack for
                             * hot module replacement to work!
                             * If we didn't, then this wouldn't work:
                             * import x from './x'
                             * module.hot.accept('./x', () => replaceCode(x))
                             * x would still reference the old module.
                            */
                            modules: false,
                            targets: { browsers: packageJson.browserslist },
                            useBuiltIns: 'entry'
                        }]
                    ],
                    plugins: ['react-hot-loader/babel']
                }
            }, {
                test: /\.scss$/,
                exclude: /node_modules/,
                use: [
                    "style-loader", // creates style nodes from JS strings
                    "css-loader", // translates CSS into CommonJS
                    "sass-loader" // compiles Sass to CSS
                ]
            }, {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: 'file-loader?mimetype=image/svg+xml'
            }, {
                test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file-loader?mimetype=application/font-woff"
            }, {
                test: /\.woff2(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file-loader?mimetype=application/font-woff"
            }, {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file-loader?mimetype=application/octet-stream"
            }, {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file-loader"
            }
        ]
    },
    devtool: 'source-map',
    context: __dirname,
    resolve: {
        extensions: ['.js', '.jsx']
    },

    serve: {
        publicPath: '/',
        add: (app, middleware, options) => {
            app.use(convert(history()))
        }
    }
}
